#pragma once

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <QVector3D>

namespace mesh {

  struct base_traits : public OpenMesh::DefaultTraits
  {
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
  };

  using base = OpenMesh::TriMesh_ArrayKernelT<base_traits>;

  struct base_vertex_color_pack
  {
    QVector3D vertex;
    QVector3D color;

    inline base_vertex_color_pack( QVector3D && _vertex, QVector3D && _color ) : vertex( std::move( _vertex ) ), color( std::move( _color ) ) {}
    inline base_vertex_color_pack( QVector3D _vertex, QVector3D _color ) : vertex( std::move( _vertex ) ), color( std::move( _color ) ) {}

    inline base_vertex_color_pack( const OpenMesh::Vec3f & openmesh_vertex, const base::Color & openmesh_color ) :
      vertex( openmesh_vertex[0], openmesh_vertex[1], openmesh_vertex[2] ),
      color( openmesh_color[0], openmesh_color[1], openmesh_color[2] ) {}
  };


}
