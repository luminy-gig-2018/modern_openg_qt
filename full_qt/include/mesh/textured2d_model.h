#pragma once

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <QVector2D>
#include <QVector3D>

namespace mesh {

  struct textured2d_model_traits : public OpenMesh::DefaultTraits
  {
    VertexAttributes( OpenMesh::Attributes::TexCoord2D );
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
  };

  using textured2d_model = OpenMesh::TriMesh_ArrayKernelT<textured2d_model_traits>;


  struct textured2d_vertex_pack
  {
    QVector3D vertex;
    QVector2D uv;

    inline textured2d_vertex_pack( QVector3D && _vertex, QVector2D && _uv ) : vertex( std::move( _vertex ) ), uv( std::move( _uv ) ) {}
    inline textured2d_vertex_pack( QVector3D _vertex, QVector2D _uv ) : vertex( std::move( _vertex ) ), uv( std::move( _uv ) ) {}

    inline textured2d_vertex_pack( const OpenMesh::Vec3f & openmesh_vertex, const OpenMesh::Vec2f & openmesh_uv ) :
      vertex( openmesh_vertex[0], openmesh_vertex[1], openmesh_vertex[2] ),
      uv( openmesh_uv[0], openmesh_uv[1] ) {}
  };


}
