#include <wrapper/qistream.h>

#include <QFile>
#include <QFileInfo>
#include <QDataStream>
#include <QTextStream>

#include <string>
#include <stdexcept>
#include <memory>

namespace wrapper {

  struct qreadfile
  {
    QFileInfo file_info;
    QFile file;

    inline qreadfile( const std::string & path ) :
      file_info( QString::fromStdString( path ) ),
      file( QString::fromStdString( path ) )
      //
    {

      //
      const bool exist_file { file_info.exists() && file_info.isFile() };
      if ( !exist_file )
        throw std::runtime_error( "not found internal file : " + path );

      //
      if ( !file.open( QIODevice::OpenModeFlag::ReadOnly ) )
        throw std::runtime_error( "can't open ressource in readonly mode : " + path );

      else if ( !file.isOpen() || !file.isReadable() )
        throw std::runtime_error( "resource is not open or not readable : " + path );

    }

    inline std::unique_ptr<wrapper::qistream> get_stream() { return std::make_unique<wrapper::qistream>( file ); }
    inline std::string get_string() { QTextStream qts( &file ); return qts.readAll().toStdString(); }

  };

}
