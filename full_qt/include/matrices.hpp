#pragma once

#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QVector3D>
#include <QtMath>

#include <memory>
#include <ostream>
#include <iostream>

inline void dump( std::ostream & os, const QMatrix4x4 & m )
{
  os << " [" << std::endl
     << "  [" << m( 0, 0 ) << "," << m( 0, 1 ) << "," << m( 0, 2 ) << "," << m( 0, 3 ) << "]" << std::endl
     << "  [" << m( 1, 0 ) << "," << m( 1, 1 ) << "," << m( 1, 2 ) << "," << m( 1, 3 ) << "]" << std::endl
     << "  [" << m( 2, 0 ) << "," << m( 2, 1 ) << "," << m( 2, 2 ) << "," << m( 2, 3 ) << "]" << std::endl
     << "  [" << m( 3, 0 ) << "," << m( 3, 1 ) << "," << m( 3, 2 ) << "," << m( 3, 3 ) << "]" << std::endl
     << " ]" << std::endl;
}


namespace matrices {

  struct model
  {
   public:
    QVector3D position;
    QVector3D rotation;
    QVector3D scale;

   public:
    inline model() {reset();}
    inline model( const QVector3D _position, const QVector3D _rotation, const QVector3D _scale ) :
      position( std::move( _position ) ), rotation( std::move( _rotation ) ), scale( std::move( _scale ) )
    { compute(); }

   public:
    QMatrix4x4 m;

   public:
    std::shared_ptr<model> default_m_up;
    inline void set_default( const model & d ) { default_m_up = std::make_unique<model>( d ); default_m_up->compute(); }

   public:
    inline void reset();
    inline void compute() {
      m.setToIdentity();
      m.scale( scale );
      m.translate( position );
      // Order YZX is convention :
      m.rotate( qRadiansToDegrees( rotation.y() ), QVector3D( 0.f, 1.f, 0.f ) );
      m.rotate( qRadiansToDegrees( rotation.z() ), QVector3D( 0.f, 0.f, 1.f ) );
      m.rotate( qRadiansToDegrees( rotation.x() ), QVector3D( 1.f, 0.f, 0.f ) );
    }
  };


  // ---- ----

  static const model default_model { { 0.f, 0.f, 0.f }, { 0.f, 0.f, 0.f }, { 1.f, 1.f, 1.f } };
  inline void model::reset() { if ( default_m_up ) { auto k( default_m_up ); *this = *default_m_up; default_m_up = k; } else *this = default_model; }

  // ---- ---- ---- ----

  struct view
  {
   public:
    QVector3D position;  // La caméra est à ..., dans l'espace monde
    QVector3D direction; // et regarde l'origine
    QVector3D angle;     // La tête est vers le haut (utilisez 0,-1,0 pour regarder à l'envers)

   public:
    inline view() {reset();}
    inline view( const QVector3D _position, const QVector3D _direction, const QVector3D _angle ) :
      position( std::move( _position ) ), direction( std::move( _direction ) ), angle( std::move( _angle ) )
    { compute(); }

   public:
    QMatrix4x4 m;

   public:
    inline operator const QMatrix4x4 & () const { return m; }

   public:
    std::shared_ptr<view> default_m_up;
    inline void set_default( const view & d ) { default_m_up = std::make_unique<view>( d ); default_m_up->compute(); }

   public:
    inline void reset();
    inline void compute() {
      m.setToIdentity();
      m.lookAt( position, direction, angle );
    }
  };

  // ---- ----

  static const view default_view { { 0.f, 0.f, 8.f }, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f } };
  inline void view::reset() { if ( default_m_up ) { auto k( default_m_up ); *this = *default_m_up; default_m_up = k; } else *this = default_view; }

  // ---- ---- ---- ----

  struct projection
  {
   public:
    float aspect;
    float fov;      /**< @todo : fov for perspective only ?  */
    float zoom;     /**< @todo : zoom for ortho only ?       */
    bool ortho_mode;

   public:
    float znear, zfar;

   public:
    inline projection() {reset();}
    inline projection( const float _aspect, const float _fov, const float _zoom, const bool _ortho_mode, const float _znear, const float _zfar ) :
      aspect( std::move( _aspect ) ), fov( std::move( _fov ) ), zoom( std::move( _zoom ) ),
      ortho_mode( std::move( _ortho_mode ) ),
      znear( std::move( _znear ) ), zfar( std::move( _zfar ) )
    { compute(); }

   public:
    QMatrix4x4 m;

   public:
    inline operator const QMatrix4x4 & () const { return m; }

   public:
    std::shared_ptr<projection> default_m_up;
    inline void set_default( const projection & d ) { default_m_up = std::make_unique<projection>( d ); default_m_up->compute(); }

   public:
    inline void reset();
    inline void compute() {
      m.setToIdentity();
      if ( ortho_mode )
        m.ortho(
          -zoom * aspect, zoom * aspect,
          -zoom, zoom,
          znear, zfar
        );
      else
        m.perspective(
          fov,
          aspect,
          znear, zfar
        );
    }
  };

  static const projection default_projection { 4.f / 3.f, 45.f, 5.f, false, 0.001f, 1000.f };
  inline void projection::reset() { if ( default_m_up ) { auto k( default_m_up ); *this = *default_m_up; default_m_up = k; } else *this = default_projection; }

  // ---- ---- ---- ----

  struct view_projection
  {
   public:
    matrices::projection & projection;
    matrices::view view;

   public:
    view_projection( matrices::projection & _projection ) :
      projection( _projection )
    { vp_compute(); }

   public:
    QMatrix4x4 vp;

   public:
    inline void matrices_compute() { projection.compute(); view.compute(); }
    inline void vp_compute() { vp =  projection.m * view.m; }
    inline const QMatrix4x4 & get_vp_compute() { vp_compute(); return vp; }

   public:
    inline void reset() { projection.reset(); view.reset(); vp_compute(); }
    inline void compute() { matrices_compute(); vp_compute(); }
  };
}

