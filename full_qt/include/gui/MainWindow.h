#pragma once

#include <QMainWindow>

namespace Ui {
  class MainWindow;
}

namespace gui {

  class MainWindow : public QMainWindow
  {
    Q_OBJECT

   protected:
    bool is_initialized = false;

   public:
    explicit MainWindow( QWidget * parent = nullptr );
    ~MainWindow();
    void showEvent( QShowEvent * event );

   protected:
    void load_internal_mesh( const std::string & mesh_file );

   private slots:
    void on_actionOpenMesh_triggered();
    void on_actionOpenInternalBunny_triggered();
    void on_actionComputeInternalTriangle_triggered();

   private slots:
    void on_checkBox_DrawWireframe_stateChanged( int arg1 );

   private:
    Ui::MainWindow * const ui;
  };

}
