#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>

#include <matrices.hpp>

#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>

#include <mesh/base.h>
#include <mesh/textured2d_model.h>

#include <OpenMesh/Core/Geometry/VectorT.hh>

#include <QDebug>
#include <QGLWidget>
#include <QMouseEvent>

#include <memory>
#include <vector>

/**
 * @todo review all concept with Object system
 */

namespace gui {

  constexpr float TRACKBALL_RADIUS = 0.6f;

  class MeshViewerWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
  {
    Q_OBJECT
   public:
    MeshViewerWidget( QWidget * _parent = nullptr );

   protected:
    // transformation
    struct _camera
    {
      matrices::model model;
      matrices::view view;
      matrices::projection projection;

      _camera();
      inline QMatrix4x4 compute() const { return projection.m * view.m * model.m; }
      inline void reset() { projection.reset(); view.reset(); model.reset(); }
    }
    camera;

   protected:
    struct default_program
    {
     public:
      QOpenGLShaderProgram program;
      QOpenGLVertexArrayObject vao;
      uint count = 0;
     public:
      virtual ~default_program() = default;
     public:
      void build( const QString & vert_path, const QString & frag_path );
      inline virtual void bind() { program.bind(); vao.bind(); }
      inline virtual void release() { vao.release(); program.release(); }
    };

   protected:
    struct _programblock_by_vertex : public default_program
    {
     public:
      QOpenGLBuffer vertex;
     public:
      virtual ~_programblock_by_vertex() = default;
    }
    programblock_by_vertex;

    struct _programblock_by_vertex_color : public default_program
    {
     public:
      QOpenGLBuffer vertex_color;
     public:
      virtual ~_programblock_by_vertex_color() = default;
    }
    programblock_by_vertex_color;

    struct _programblock_by_vertex_uv : public default_program
    {
     public:
      QOpenGLBuffer vertex_uv;
      std::unique_ptr<QOpenGLTexture> texture_up;
     public:
      virtual ~_programblock_by_vertex_uv() = default;
      inline virtual void bind() {
        default_program::bind();
        if ( texture_up ) {
          texture_up->bind( 0 );
          program.setUniformValue( "uniform_sampler2d_texture", 0 );
        }
      }
      inline virtual void release() {
        if ( texture_up )
          texture_up->release();
        default_program::release();
      }
    }
    programblock_by_vertex_uv;

   protected:
    QOpenGLBuffer buffer_index{QOpenGLBuffer::Type::IndexBuffer};
    uint buffer_index_count = 0;
    bool draw_use_index_buffer = false;

   protected:
    GLenum draw_primitive_type = 0;
    default_program * draw_programblock_p = nullptr;

    inline void unset_draw() {
      draw_use_index_buffer = false;
      draw_primitive_type = 0;
      buffer_index_count = 0;
      draw_programblock_p = nullptr;
    }

   private:
    void initialise_program();

   private:
    bool opengl_initialized = false;

   public:
    inline bool opengl_is_initialized() const { return opengl_initialized; }
    void throw_if_opengl_not_initialized() const;

   public:
    bool paint_wireframe = true;

   protected:
    // events GL
    void initializeGL();
    void resizeGL( int _w, int _h );
    void paintGL();

   protected:
    QPoint           mouse_last_position;

   protected:
    // Qt mouse events
    virtual void mousePressEvent( QMouseEvent * );
    virtual void mouseReleaseEvent( QMouseEvent * );
    virtual void mouseMoveEvent( QMouseEvent * );
    virtual void wheelEvent( QWheelEvent * );

   private:
    template<typename TMesh>
    void internal_load_mesh_index( const TMesh & mesh );

   public:
    void load_mesh( const std::vector<QVector3D> & vertices );
    void load_mesh( const mesh::base & mesh );
    void load_mesh( const mesh::textured2d_model & mesh, const std::string & base_texture_directory );

  };

}
