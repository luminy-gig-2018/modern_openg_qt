#
# ModernOpengQt Project
# GIG 2018 Team

#
##
QT       += core gui widgets
QT       += opengl

#
##
TARGET = ModernOpengFullQt
TEMPLATE = app

#
##
QMAKE_CXXFLAGS += $$(CXXFLAGS)
QMAKE_CFLAGS   += $$(CFLAGS)
QMAKE_LFLAGS   += $$(LDFLAGS)

#
## for release only :
CONFIG(release, debug|release) {
  QMAKE_CXXFLAGS += $$(CXXFLAGS) -O3 -fno-builtin
  QMAKE_CFLAGS   += $$(CFLAGS)   -O3 -fno-builtin
  QMAKE_LFLAGS   += $$(LDFLAGS)  -s
}

#
##
LIBS += -lOpenMeshCore
# INCLUDEPATH += $$system(pg_config --includedir)

message( "NOTE : LIBS AND INCLUDEPATH for LIBS are not" )
message( "         properly configured, you need to change it !" )

#
##
INCLUDEPATH += include/

#
##
HEADERS += $$files(include/*.h, true)
HEADERS += $$files(include/*.hpp, true)

SOURCES += $$files(src/*.c, true)
SOURCES += $$files(src/*.cpp, true)

RESOURCES += glsl/glsl.qrc mesh/mesh.qrc

FORMS += $$files(ui/*.ui, true)






