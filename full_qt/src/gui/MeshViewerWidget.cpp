#include <gui/MeshViewerWidget.h>

#include <stdexcept>

namespace gui {


  MeshViewerWidget::MeshViewerWidget( QWidget * _parent ) :
    QOpenGLWidget( _parent )
  {
    camera.reset();

    setMouseTracking( true );
    setFocus();
  }

  // ---- ---- ---- ----

  MeshViewerWidget::_camera::_camera()
  {
    // default view
    {
      matrices::view default_view( matrices::default_view );
      default_view.position.setZ( 1.f );
      view.set_default( default_view );
      view.reset();
    }
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::default_program::build( const QString & vert_path, const QString & frag_path )
  {
    //
    program.create();

    // Compile vertex shader
    if ( !program.addShaderFromSourceFile( QOpenGLShader::Vertex, vert_path ) )
      throw std::runtime_error( ( "Can't compile vertex file : " + vert_path ).toStdString() );

    // Compile fragment shader
    if ( !program.addShaderFromSourceFile( QOpenGLShader::Fragment, frag_path ) )
      throw std::runtime_error( ( "Can't compile vertex file : " + frag_path ).toStdString() );

    // Link shader pipeline
    if ( !program.link() )
      throw std::runtime_error( ( "Can't link program of vertex(" + vert_path + ") frag(" + frag_path + ")" ).toStdString() );

    // Bind shader pipeline for use
    if ( !program.bind() )
      throw std::runtime_error( ( "Can't bind program of vertex(" + vert_path + ") frag(" + frag_path + ")" ).toStdString() );
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::initialise_program()
  {

    {
      programblock_by_vertex.build(
        ":/glsl/in_vec3.vert",
        ":/glsl/uniform_color__out_color.frag"
      );
      programblock_by_vertex.vertex.create();
      programblock_by_vertex.vao.create();

      programblock_by_vertex.vao.bind();
      glEnableVertexAttribArray( 0 );
      programblock_by_vertex.vertex.bind();

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer :
      glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, nullptr );

      programblock_by_vertex.vao.release();
      // After vao release :
      programblock_by_vertex.vertex.release();
      glDisableVertexAttribArray( 0 );
    }

    {
      programblock_by_vertex_color.build(
        ":/glsl/in_vec3_color__out_color.vert",
        ":/glsl/in_color__out_color.frag"
      );
      programblock_by_vertex_color.vertex_color.create();
      programblock_by_vertex_color.vao.create();

      programblock_by_vertex_color.vao.bind();
      glEnableVertexAttribArray( 0 );
      glEnableVertexAttribArray( 1 );
      programblock_by_vertex_color.vertex_color.bind();

      const uint buffer_stride( sizeof( mesh::base_vertex_color_pack ) );
      const void * const vertex_offset( reinterpret_cast<void *>( 0 ) );
      const void * const color_offset( reinterpret_cast<void *>( sizeof( QVector3D ) ) );

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer:
      glVertexAttribPointer( 0, 3, GL_FLOAT, false, buffer_stride, vertex_offset );
      // glsl : layout( location = 1 ) in vec3 color_from_buffer:
      glVertexAttribPointer( 1, 3, GL_FLOAT, false, buffer_stride, color_offset );

      programblock_by_vertex_color.vao.release();
      // After vao release :
      programblock_by_vertex_color.vertex_color.release();
      glDisableVertexAttribArray( 0 );
      glDisableVertexAttribArray( 1 );
    }

    {
      programblock_by_vertex_uv.build(
        ":/glsl/in_vec3_uv__out_uv.vert",
        ":/glsl/in_uv__out_color.frag"
      );

      programblock_by_vertex_uv.vertex_uv.create();
      programblock_by_vertex_uv.vao.create();

      programblock_by_vertex_uv.vao.bind();
      glEnableVertexAttribArray( 0 );
      glEnableVertexAttribArray( 1 );
      programblock_by_vertex_uv.vertex_uv.bind();

      const uint buffer_stride( sizeof( mesh::textured2d_vertex_pack ) );
      const void * const vertex_offset( reinterpret_cast<void *>( 0 ) );
      const void * const uv_offset( reinterpret_cast<void *>( sizeof( QVector3D ) ) );

      // glsl : layout( location = 0 ) in vec3 vertex_from_buffer:
      glVertexAttribPointer( 0, 3, GL_FLOAT, false, buffer_stride, vertex_offset );
      // glsl : layout( location = 1 ) in vec2 uv_from_buffer:
      glVertexAttribPointer( 1, 2, GL_FLOAT, false, buffer_stride, uv_offset );

      programblock_by_vertex_uv.vao.release();
      // After vao release :
      programblock_by_vertex_uv.vertex_uv.release();
      glDisableVertexAttribArray( 0 );
      glDisableVertexAttribArray( 1 );
    }

    {
      buffer_index.create();
    }

  }

  // ---- ---- ---- ----

  void MeshViewerWidget::throw_if_opengl_not_initialized() const
  {
    if ( !opengl_initialized )
      throw std::runtime_error( "OpenGL is not initialized" );
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::initializeGL()
  {
    initializeOpenGLFunctions();

    //
    glEnable( GL_MULTISAMPLE ); // for Anti-Aliasing
    glEnable( GL_DEPTH_TEST );

    // Configure render pipeline
    initialise_program();

    //
    glClearColor( 1.0, 1.0, 1.0, 1.0 );

    //
    opengl_initialized = true;
  }


  void MeshViewerWidget::resizeGL( int _w, int _h )
  {
    camera.projection.aspect = float( _w ) / float( _h >= 1.f ? _h : 1.f );
    camera.projection.compute();
    glViewport( 0, 0, _w, _h );
    repaint();
  }



  void MeshViewerWidget::paintGL()
  {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );


    // No draw needs
    if ( draw_primitive_type == 0 || draw_programblock_p == nullptr )
      return;

    // Strange index draw
    if ( draw_use_index_buffer && buffer_index_count == 0 )
      return;

    //
    default_program & programblock( *draw_programblock_p );

    //
    if ( programblock.count == 0 )
      return;

    //
    programblock.bind();

    programblock.program.setUniformValue( "uniform_mvp", camera.compute() );

    //
    if ( draw_use_index_buffer )
    {
      buffer_index.bind();

      programblock.program.setUniformValue( "uniform_mode_wireframe", int( 0 ) );
      glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      glDrawElements( draw_primitive_type, GLint( buffer_index_count ), GL_UNSIGNED_INT, nullptr );

      if ( paint_wireframe )
      {
        programblock.program.setUniformValue( "uniform_mode_wireframe", int( 1 ) );
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        glDrawElements( draw_primitive_type, GLint( buffer_index_count ), GL_UNSIGNED_INT, nullptr );
      }

      buffer_index.release();
    }
    else
    {
      programblock.program.setUniformValue( "uniform_mode_wireframe", int( 0 ) );
      glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      glDrawArrays( draw_primitive_type, 0, GLint( programblock.count ) );

      if ( paint_wireframe )
      {
        programblock.program.setUniformValue( "uniform_mode_wireframe", int( 1 ) );
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        glDrawArrays( draw_primitive_type, 0, GLint( programblock.count ) );
      }
    }

    //
    programblock.release();

  }

  // ---- ---- ---- ----


  void MeshViewerWidget::mousePressEvent( QMouseEvent * _event )
  {
    mouse_last_position = _event->pos();
  }

  void MeshViewerWidget::mouseMoveEvent( QMouseEvent * event )
  {
    static const float mouse_ratio( 0.005f );

    //
    const QPoint mouse_new_position( event->pos() );

    //
    const QVector2D mouse_distance(
      mouse_new_position.x() - mouse_last_position.x(),
      mouse_new_position.y() - mouse_last_position.y()
    );

    //
    mouse_last_position = mouse_new_position;

    if ( ( event->buttons() == ( Qt::LeftButton + Qt::MidButton ) ) || ( event->buttons() == Qt::LeftButton && event->modifiers() == Qt::ControlModifier ) )
    {
      const float h( height() );
      const float value_y( mouse_distance.y() * 3.f / h );
      camera.view.position += QVector3D( 0.f, 0.f, value_y );
      camera.view.compute();
    }
    else if ( ( event->buttons() == Qt::MidButton ) || ( event->buttons() == Qt::LeftButton && event->modifiers() == Qt::AltModifier ) )
    {
      camera.model.position += QVector3D( mouse_distance.x() * mouse_ratio, -mouse_distance.y() * mouse_ratio, 0.f );
      camera.model.compute();
      /**< @todo implement real mouse 3d mouvement */
    }
    else if ( event->buttons() == Qt::RightButton )
    {
      // right click move ?
    }
    else if ( event->buttons() == Qt::LeftButton )
    {
      // axes vector rotation :
      camera.model.rotation += QVector3D( mouse_distance.y() * mouse_ratio, mouse_distance.x() * mouse_ratio, 0.f );
      camera.model.compute();
      /**< @todo implement quaternion */
    }
    repaint();
  }

  void MeshViewerWidget::mouseReleaseEvent( QMouseEvent * /* _event */ )
  {
  }

  void MeshViewerWidget::wheelEvent( QWheelEvent * _event )
  {
    float d = -float( _event->delta() ) / 120.f * 0.2f;
    camera.view.position += QVector3D( 0.f, 0.f, d );
    camera.view.compute();
    repaint();
    _event->accept();
  }


}
