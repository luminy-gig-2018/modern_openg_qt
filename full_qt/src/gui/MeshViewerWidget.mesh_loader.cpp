#include <gui/MeshViewerWidget.h>

#include <stdexcept>

#include <wrapper/qreadfile.h>

#include <QVector2D>
#include <QVector3D>
#include <QImage>

namespace gui {

  template<typename TMesh>
  void MeshViewerWidget::internal_load_mesh_index( const TMesh & mesh )
  {
    buffer_index_count = GLuint( mesh.n_faces() * 3 );
    std::vector<GLuint> index_data;
    index_data.reserve( buffer_index_count );

    //
    mesh::textured2d_model::ConstFaceIter face_it( mesh.faces_begin() );
    const mesh::textured2d_model::ConstFaceIter face_end_it( mesh.faces_end() );
    for ( ; face_it != face_end_it; ++face_it )
    {
      mesh::textured2d_model::ConstFaceVertexIter face_vertex_it( mesh.cfv_iter( *face_it ) );
      index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
      ++face_vertex_it;
      index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
      ++face_vertex_it;
      index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
    }

    //
    const GLuint index_sizeof( buffer_index_count * sizeof( GLuint ) );

    buffer_index.bind();
    buffer_index.allocate( index_data.data(), int( index_sizeof ) );
    buffer_index.release();

    draw_use_index_buffer = true;
  }


  // ---- ---- ---- ----

  void MeshViewerWidget::load_mesh( const std::vector<QVector3D> & vertices )
  {
    throw_if_opengl_not_initialized();
    unset_draw();

    // BufferData of Vertex
    {
      programblock_by_vertex.count = GLuint( vertices.size() );
      const GLuint vertex_sizeof( programblock_by_vertex.count * sizeof( QVector3D ) );

      programblock_by_vertex.vertex.bind();
      programblock_by_vertex.vertex.allocate( vertices.data(), int( vertex_sizeof ) );
      programblock_by_vertex.vertex.release();
    }

    draw_primitive_type = GL_TRIANGLES;
    draw_programblock_p = &programblock_by_vertex;

    repaint();
  }

  // ---- ----

  void MeshViewerWidget::load_mesh( const mesh::base & mesh )
  {
    throw_if_opengl_not_initialized();
    unset_draw();

    // BufferData of Index
    internal_load_mesh_index( mesh );

    //
    // BufferData of Vertex
    {
      programblock_by_vertex.count = GLuint( mesh.n_vertices() );
      const GLuint vertex_sizeof( programblock_by_vertex.count * sizeof( QVector3D ) );

      programblock_by_vertex.vertex.bind();
      programblock_by_vertex.vertex.allocate( mesh.points(), int( vertex_sizeof ) );
      programblock_by_vertex.vertex.release();
    }

    draw_primitive_type = GL_TRIANGLES;
    draw_programblock_p = &programblock_by_vertex;

    repaint();
  }

  // ---- ----

  void MeshViewerWidget::load_mesh( const mesh::textured2d_model & mesh, const std::string & base_mesh_directory )
  {
    throw_if_opengl_not_initialized();
    unset_draw();

    // BufferData of Index
    internal_load_mesh_index( mesh );

    // BufferData of Vertex / UV
    /**< @todo HOW Extract properly mesh Vertex UV (tex_coord_2d) witout foreach all vertex ? */
    {
      programblock_by_vertex_uv.count = GLuint( mesh.n_vertices() );
      std::vector<mesh::textured2d_vertex_pack> vertex_uv_data;
      vertex_uv_data.reserve( programblock_by_vertex_uv.count );

      //
      mesh::textured2d_model::VertexIter vertex_it( mesh.vertices_begin() );
      const mesh::textured2d_model::VertexIter vertex_end_it( mesh.vertices_end() );
      for ( ; vertex_it != vertex_end_it; ++vertex_it )
      {
        vertex_uv_data.emplace_back(
          mesh.point( *vertex_it ),      // glsl : vec3 vertex_from_buffer;
          mesh.texcoord2D( *vertex_it )  // glsl : vec2 uv_from_buffer;
        );
        /**< @todo Check if  mesh.texcoord2D( *vertex_it ) is good ? */
      }

      //
      const GLuint vertex_uv_sizeof( programblock_by_vertex_uv.count * sizeof( mesh::textured2d_vertex_pack ) );

      programblock_by_vertex_uv.vertex_uv.bind();
      programblock_by_vertex_uv.vertex_uv.allocate( vertex_uv_data.data(), int( vertex_uv_sizeof ) );
      programblock_by_vertex_uv.vertex_uv.release();
    }

    std::cout << "base_mesh_directory : " << base_mesh_directory << std::endl;
    std::cout << "loading texture ..." << std::endl;

    // Texture
    {
      OpenMesh::MPropHandleT< std::map< int, std::string >> texture_property;
      std::string first_texture_entry;
      if ( mesh.get_property_handle( texture_property, "TextureMapping" ) )
        if ( !mesh.property( texture_property ).empty() )
          first_texture_entry = mesh.property( texture_property ).begin()->second;
      if ( first_texture_entry.empty() )
        std::cout << "  no texture property found." << std::endl;
      else
      {
        if ( mesh.property( texture_property ).size() > 1 )
          for ( const std::pair<int, std::string> & el : mesh.property( texture_property ) )
            std::cout << "  texture_entry n" << el.first << " : " << el.second << std::endl;
        std::cout << "  selected texture : " << first_texture_entry << std::endl;


        const std::string texture_path( base_mesh_directory + "/" + first_texture_entry );
        std::cout << "  opening texture_path : " << texture_path << std::endl;


        // Opening it
        QImage image( QString::fromStdString( texture_path ) );
        QImage::Format format( image.format() );

        if ( format == QImage::Format_Invalid )
          std::cout << "  !!! No texture found !!! " << std::endl;
        else
        {
          //
          std::cout << "  Image size " << image.width() << "x" << image.height() << std::endl;

          //
          programblock_by_vertex_uv.texture_up =
            std::make_unique<QOpenGLTexture>(
              image.mirrored()
            );

          //
          programblock_by_vertex_uv.texture_up->setMinificationFilter( QOpenGLTexture::Nearest );
          programblock_by_vertex_uv.texture_up->setMagnificationFilter( QOpenGLTexture::Linear );
          programblock_by_vertex_uv.texture_up->setWrapMode( QOpenGLTexture::Repeat );
        }
      }
    }

    draw_primitive_type = GL_TRIANGLES;
    draw_programblock_p = &programblock_by_vertex_uv;

    repaint();
  }

}
