#include <gui/MainWindow.h>

#include <QApplication>
#include <QSurfaceFormat>

#include <stdexcept>
#include <cstdlib>
#include <iostream>

int main( int argc, char * argv[] )
{
  try
  {
    //
    QApplication app( argc, argv );

    //
    QSurfaceFormat format;
    format.setDepthBufferSize( 24 );
    QSurfaceFormat::setDefaultFormat( format );

    //
    app.setApplicationName( "DemoFullModernOpenGL" );
    app.setApplicationVersion( "0.1" );

    //
    gui::MainWindow w;
    w.show();

    return app.exec();
  }
  catch ( const std::exception & e )
  {
    std::cerr << "Error : " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
}
