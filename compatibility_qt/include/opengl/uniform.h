#pragma once

#include <GL/glew.h>

#include <string>
#include <stdexcept>

#include <glm/glm.hpp>

namespace opengl {

  /**
  * @see http://www.opengl-tutorial.org/fr/beginners-tutorials/tutorial-3-matrices/
  */
  inline GLint get_uniform( GLuint program_id, const char * uniform_var_name )
  {
    const GLint id( glGetUniformLocation( program_id, uniform_var_name ) );
    if ( id < 0 )
      throw std::runtime_error(
        "Can't found unifrom variable '" + std::string( uniform_var_name )
        + "' on program_id(" + std::to_string( program_id ) + ")"
      );
    return id;
  }

  /**
   * Important ! Program need to be "used" (glUseProgram) to use set_uniform_value
   * @defgroup set_uniform_value Setting Uniforms by polymorphism
   * @{
   */

  inline void set_uniform_value( const GLint uniform_id, const float v )  { glUniform1f( uniform_id, v ); }
  inline void set_uniform_value( const GLint uniform_id, const int v )    { glUniform1i( uniform_id, v ); }
  inline void set_uniform_value( const GLint uniform_id, const unsigned int v )   { glUniform1ui( uniform_id, v ); }

  inline void set_uniform_value( const GLint uniform_id, const glm::vec2 & v )   { glUniform2fv( uniform_id, 1, &v[0] ); }
  inline void set_uniform_value( const GLint uniform_id, const glm::vec3 & v )   { glUniform3fv( uniform_id, 1, &v[0] ); }
  inline void set_uniform_value( const GLint uniform_id, const glm::vec4 & v )   { glUniform4fv( uniform_id, 1, &v[0] ); }
  inline void set_uniform_value( const GLint uniform_id, const glm::mat4x4 & v ) { glUniformMatrix4fv( uniform_id, 1, GL_FALSE, &v[0][0] ); }

  /** @} */


}
