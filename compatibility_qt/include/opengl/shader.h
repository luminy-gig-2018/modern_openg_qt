#pragma once

#include <GL/glew.h>

#include <string>

namespace opengl {

  /**
  * @see http://www.opengl-tutorial.org/fr/beginners-tutorials/tutorial-2-the-first-triangle/
  */
  GLuint new_shader( const std::string & shader_source, const GLenum shader_type );

}
