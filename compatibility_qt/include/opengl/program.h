#pragma once

#include <GL/glew.h>

#include <string>

namespace opengl {

  /**
  * @see http://www.opengl-tutorial.org/fr/beginners-tutorials/tutorial-2-the-first-triangle/
  */
  GLuint new_program( const std::string & vertex_source, const std::string & fragment_source );

}
