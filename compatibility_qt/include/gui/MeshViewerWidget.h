#ifndef MESHVIEWERWIDGET_H
#define MESHVIEWERWIDGET_H

#include <GL/glew.h>

#include <matrices.hpp>

#include <mesh/base.h>
#include <mesh/textured2d_model.h>

#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <glm/glm.hpp>

#define GL_GLEXT_PROTOTYPES
#include <QDebug>
#include <QGLWidget>
#include <QMouseEvent>

/**
 * @todo review all concept with Object system
 */

namespace gui {

  constexpr float TRACKBALL_RADIUS = 0.6f;

  class MeshViewerWidget : public QGLWidget
  {
    Q_OBJECT
   public:
    MeshViewerWidget( QWidget * _parent = nullptr );
    MeshViewerWidget( QGLFormat & _fmt, QWidget * _parent );

   protected:
    // transformation
    struct _camera
    {
      matrices::model model;
      matrices::view view;
      matrices::projection projection;

      _camera();
      inline glm::mat4 compute() const { return projection.m * view.m * model.m; }
    }
    camera;

   protected:
    struct _program_base
    {
      // glsl program
      GLuint id = 0;

      // uniforms on glsl program
      GLint uniform_mvp_id = -1;
      GLint uniform_mode_wireframe_id = -1;
      GLint uniform_color_id = -1;

      // vao (initialized by buffer)
      GLuint vao_vertex_id = 0;
      GLuint vao_vertex_index_id = 0;
    }
    glsl_program_base;

    struct _program_textured2d
    {
      //
      static const GLenum sampler2d_texture_num = GL_TEXTURE10;

      // glsl program
      GLuint id = 0;

      // uniforms on glsl program
      GLint uniform_mvp_id = -1;
      GLint uniform_mode_wireframe_id = -1;
      GLint uniform_sampler2d_texture_id = -1;

      // vao (initialized by buffer)
      GLuint vao_vertex_uv_index_id = 0;
    }
    glsl_program_textured2d;

    enum class glsl_program_modes { none, base, base_not_indexed, textured2d }
    glsl_program_mode { glsl_program_modes::none };

   protected:
    struct _buffer
    {
      // For glsl_program_base
      GLuint vertex_id = 0;
      GLuint vertex_count = 0;

      // For glsl_program_textured2d
      GLuint vertex_uv_id = 0;
      GLuint vertex_uv_count = 0;
      GLuint texture_id = 0;

      // For all program
      GLuint index_id = 0;
      GLuint index_count = 0;
    }
    buffer;

   private:
    void initialise_program();
    void initialise_buffer();

   private:
    bool opengl_initialized = false;

   public:
    inline bool opengl_is_initialized() const { return opengl_initialized; }
    void throw_if_opengl_not_initialized() const;

   public:
    bool paint_wireframe = true;

   protected:
    // events GL
    void initializeGL();
    void resizeGL( int _w, int _h );
    void paintGL();

   public:
    void load_mesh( const std::vector<glm::vec3> & vertices );
    void load_mesh( const mesh::base & mesh );
    void load_mesh( const mesh::textured2d_model & mesh, const std::string & base_texture_directory );

  protected:
    QPoint           mouse_last_position;

   protected:
    // Qt mouse events
    virtual void mousePressEvent( QMouseEvent * );
    virtual void mouseReleaseEvent( QMouseEvent * );
    virtual void mouseMoveEvent( QMouseEvent * );
    virtual void wheelEvent( QWheelEvent * );
  };

}

#endif // MESHVIEWERWIDGET_H
