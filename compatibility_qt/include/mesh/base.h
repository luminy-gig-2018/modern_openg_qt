#pragma once

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

namespace mesh {

  struct base_traits : public OpenMesh::DefaultTraits
  {
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
  };

  using base = OpenMesh::TriMesh_ArrayKernelT<base_traits>;

}
