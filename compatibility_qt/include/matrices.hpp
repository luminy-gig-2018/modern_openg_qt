#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <memory>
#include <ostream>
#include <iostream>

inline void dump( std::ostream & os, const glm::mat4x4 & m )
{
  os << " [" << std::endl
     << "  [" << m[0][0] << "," << m[0][1] << "," << m[0][2] << "," << m[0][3] << "]" << std::endl
     << "  [" << m[1][0] << "," << m[1][1] << "," << m[1][2] << "," << m[1][3] << "]" << std::endl
     << "  [" << m[2][0] << "," << m[2][1] << "," << m[2][2] << "," << m[2][3] << "]" << std::endl
     << "  [" << m[3][0] << "," << m[3][1] << "," << m[3][2] << "," << m[3][3] << "]" << std::endl
     << " ]" << std::endl;
}


namespace matrices {

  struct model
  {
   public:
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;

   public:
    inline model() {reset();}
    inline model( const glm::vec3 _position, const glm::vec3 _rotation, const glm::vec3 _scale ) :
      position( std::move( _position ) ), rotation( std::move( _rotation ) ), scale( std::move( _scale ) )
    { compute(); }

   public:
    glm::mat4 m;

   public:
    inline operator const glm::mat4 & () const { return m; }

   public:
    std::shared_ptr<model> default_m_up;
    inline void set_default( const model & d ) { default_m_up = std::make_unique<model>( d ); default_m_up->compute(); }

   public:
    inline void reset();
    inline void compute() {  /** @todo check order */
      m = glm::mat4( 1.f );
      m = glm::scale( m, scale );
      m = glm::translate( m, position );
      // Order YZX is convention :
      m = glm::rotate( m, rotation.y, glm::vec3( 0.f, 1.f, 0.f ) ); /**< @todo in one step ? */
      m = glm::rotate( m, rotation.z, glm::vec3( 0.f, 0.f, 1.f ) ); /**< @todo in one step ? */
      m = glm::rotate( m, rotation.x, glm::vec3( 1.f, 0.f, 0.f ) ); /**< @todo in one step ? */
    }
  };

  // ---- ----

  static const model default_model { { 0.f, 0.f, 0.f }, { 0.f, 0.f, 0.f }, { 1.f, 1.f, 1.f } };
  inline void model::reset() { if ( default_m_up ) { auto k( default_m_up ); *this = *default_m_up; default_m_up = k; } else *this = default_model; }

  // ---- ---- ---- ----

  struct view
  {
   public:
    glm::vec3 position;  // La caméra est à ..., dans l'espace monde
    glm::vec3 direction; // et regarde l'origine
    glm::vec3 angle;     // La tête est vers le haut (utilisez 0,-1,0 pour regarder à l'envers)

   public:
    inline view() {reset();}
    inline view( const glm::vec3 _position, const glm::vec3 _direction, const glm::vec3 _angle ) :
      position( std::move( _position ) ), direction( std::move( _direction ) ), angle( std::move( _angle ) )
    { compute(); }

   public:
    glm::mat4 m;

   public:
    inline operator const glm::mat4 & () const { return m; }

   public:
    std::shared_ptr<view> default_m_up;
    inline void set_default( const view & d ) { default_m_up = std::make_unique<view>( d ); default_m_up->compute(); }

   public:
    inline void reset();
    inline void compute() { m = glm::lookAt( position, direction, angle ); }
  };

  // ---- ----

  static const view default_view { { 0.f, 0.f, 8.f }, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f } };
  inline void view::reset() { if ( default_m_up ) { auto k( default_m_up ); *this = *default_m_up; default_m_up = k; } else *this = default_view; }

  // ---- ---- ---- ----

  struct projection
  {
   public:
    float aspect;
    float fov;      /**< @todo : fov for perspective only ?  */
    float zoom;     /**< @todo : zoom for ortho only ?       */
    bool ortho_mode;

   public:
    float znear, zfar;

   public:
    inline projection() {reset();}
    inline projection( const float _aspect, const float _fov, const float _zoom, const bool _ortho_mode, const float _znear, const float _zfar ) :
      aspect( std::move( _aspect ) ), fov( std::move( _fov ) ), zoom( std::move( _zoom ) ),
      ortho_mode( std::move( _ortho_mode ) ),
      znear( std::move( _znear ) ), zfar( std::move( _zfar ) )
    { compute(); }

   public:
    glm::mat4 m;

   public:
    inline operator const glm::mat4 & () const { return m; }

   public:
    std::shared_ptr<projection> default_m_up;
    inline void set_default( const projection & d ) { default_m_up = std::make_unique<projection>( d ); default_m_up->compute(); }

   public:
    inline void reset();
    inline void compute() {
      m = (
            ortho_mode ?
            glm::ortho(
              -zoom * aspect, zoom * aspect,
              -zoom, zoom,
              znear, zfar
            ) :
            glm::perspective(
              fov,
              aspect,
              znear, zfar
            )
          );
    }
  };

  static const projection default_projection { 4.f / 3.f, 45.f, 5.f, false, 0.001f, 1000.f };
  inline void projection::reset() { if ( default_m_up ) { auto k( default_m_up ); *this = *default_m_up; default_m_up = k; } else *this = default_projection; }

  // ---- ---- ---- ----

  struct view_projection
  {
   public:
    matrices::projection & projection;
    matrices::view view;

   public:
    view_projection( matrices::projection & _projection ) :
      projection( _projection )
    { vp_compute(); }

   public:
    glm::mat4 vp;

   public:
    inline void matrices_compute() { projection.compute(); view.compute(); }
    inline void vp_compute() { vp =  projection.m * view.m; }
    inline const glm::mat4 & get_vp_compute() { vp_compute(); return vp; }

   public:
    inline void reset() { projection.reset(); view.reset(); vp_compute(); }
    inline void compute() { matrices_compute(); vp_compute(); }
  };
}

