#include <gui/MainWindow.h>
#include <QApplication>

#include <stdexcept>
#include <cstdlib>
#include <iostream>

int main( int argc, char * argv[] )
{
  try
  {
    //
    QApplication a( argc, argv );

    //
    gui::MainWindow w;

    //
    w.show();

    return a.exec();
  }
  catch ( const std::exception & e )
  {
    std::cerr << "Error : " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
}
