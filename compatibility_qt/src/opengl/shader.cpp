#include <opengl/shader.h>

#include <stdexcept>
#include <sstream>
#include <vector>
#include <algorithm>

namespace opengl {


  std::string get_shader_type_name( const GLenum shader_type )
  {
    switch ( shader_type )
    {
    case GL_VERTEX_SHADER:
      return "VERTEX_SHADER";
    case GL_FRAGMENT_SHADER:
      return "FRAGMENT_SHADER";
    case GL_GEOMETRY_SHADER:
      return "GEOMETRY_SHADER";
    default:
      return "Unknown '" + std::to_string( shader_type ) + "'";
    }
  }


  /**
  * @see http://www.opengl-tutorial.org/fr/beginners-tutorials/tutorial-2-the-first-triangle/
  */
  GLuint new_shader( const std::string & code, const GLenum shader_type )
  {
    GLuint id( glCreateShader( shader_type ) );

    // Compile it
    const char * const code_cstr[1] {code.data()};
    const GLint code_length[1] { static_cast<GLint>( code.size() ) };

    glShaderSource( id, 1, code_cstr, code_length );
    glCompileShader( id );

    // Check
    int compile_status_result = GL_FALSE;
    int info_log_length { 0 };
    glGetShaderiv( id, GL_COMPILE_STATUS, &compile_status_result );
    glGetShaderiv( id, GL_INFO_LOG_LENGTH, &info_log_length );
    if ( compile_status_result == GL_FALSE )
    {
      std::vector<char> error_message( static_cast<size_t>( info_log_length ) + 1 );
      glGetShaderInfoLog( id, info_log_length, nullptr, error_message.data() );
      error_message[static_cast<size_t>( info_log_length )] = '\0';
      const std::string error_message_str( error_message.data() );

      //
      std::string code_without_carrier( code );
      std::replace( code_without_carrier.begin(), code_without_carrier.end(), '\r', ' ' );

      std::ostringstream oss;
      oss << "Error during '" << get_shader_type_name( shader_type ) << "' shader compilation : " << std::endl
          << "  " + error_message_str << std::endl
          << " Source shader : " << std::endl
          << "  -------  " << std::endl
          << code_without_carrier << std::endl
          << "  -------  " << std::endl;
      //
      throw std::runtime_error( oss.str() );
    }

    return id;
  }


}
