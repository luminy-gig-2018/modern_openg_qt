#include <opengl/program.h>
#include <opengl/shader.h>

#include <stdexcept>
#include <sstream>
#include <vector>

namespace opengl {


  /**
  * @see http://www.opengl-tutorial.org/fr/beginners-tutorials/tutorial-2-the-first-triangle/
  */
  GLuint new_program( const std::string & vertex_source, const std::string & fragment_source )
  {
    // Compile Shaders
    const GLuint vertex_id( opengl::new_shader( vertex_source, GL_VERTEX_SHADER ) );
    const GLuint fragment_id( opengl::new_shader( fragment_source, GL_FRAGMENT_SHADER ) );

    // Link the program
    const GLuint program_id( glCreateProgram() );
    glAttachShader( program_id, vertex_id );
    glAttachShader( program_id, fragment_id );
    glLinkProgram( program_id );

    // Check the program
    int link_status_result = GL_FALSE;
    int info_log_length {0};
    glGetProgramiv( program_id, GL_LINK_STATUS, &link_status_result );
    glGetProgramiv( program_id, GL_INFO_LOG_LENGTH, &info_log_length );
    if ( link_status_result == GL_FALSE )
    {
      std::vector<char> error_message( static_cast<size_t>( info_log_length ) + 1 );
      glGetProgramInfoLog( program_id, info_log_length, nullptr, error_message.data() );
      error_message[static_cast<size_t>( info_log_length )] = '\0';
      const std::string error_message_str( error_message.data() );

      //
      std::ostringstream oss;
      oss << "Error during glsl program linking : " << std::endl;
      oss << "  --- (log_length : " << info_log_length << ") ---  " << std::endl
          << "  " + error_message_str << std::endl
          << "  --- ---  " << std::endl;
      if ( info_log_length == 0 )
        oss << "  (log_length==0 : possible non-debug mode on GPU ...)" << std::endl;

      //
      throw std::runtime_error( oss.str() );
    }

    // Unlink/Detach
    glDetachShader( program_id, vertex_id );
    glDetachShader( program_id, fragment_id );

    // Freeing compiled shaders
    glDeleteShader( vertex_id );
    glDeleteShader( fragment_id );

    return program_id;
  }


}

