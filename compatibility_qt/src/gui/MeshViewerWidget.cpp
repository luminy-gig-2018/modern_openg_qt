#include <gui/MeshViewerWidget.h>

#include <opengl/program.h>
#include <opengl/uniform.h>

#include <wrapper/qreadfile.h>

#include <glm/vec3.hpp>

#include <QImage>

// #define GLM_ENABLE_EXPERIMENTAL 1
// #include <glm/gtx/norm.hpp>

namespace gui {

  /** @todo check that really work with glew */
  static QGLFormat new_default_format()
  {
    QGLFormat widget_format;

    widget_format.setDoubleBuffer( true );
    //widget_format.setDepthBufferSize( 24 );
    //widget_format.setSwapInterval( 1 );

    widget_format.setProfile( QGLFormat::CoreProfile );
    widget_format.setVersion( 3, 3 );
    widget_format.setSamples( 16 ); // Anti-Aliasing x16

    return widget_format;
  }

  static const QGLFormat default_format( new_default_format() );

  // ---- ----

  MeshViewerWidget::MeshViewerWidget( QWidget * _parent ) :
    QGLWidget( default_format, _parent )
  {
    setMouseTracking( true );
    setFocus();
  }

  MeshViewerWidget::MeshViewerWidget( QGLFormat & _fmt, QWidget * _parent ) :
    QGLWidget( _fmt, _parent )
  {
    setMouseTracking( true );
    setFocus();
  }

  // ---- ---- ---- ----

  MeshViewerWidget::_camera::_camera()
  {
    // default view
    {
      matrices::view default_view( matrices::default_view );
      default_view.position.z = 1.f;
      view.set_default( default_view );
      view.reset();
    }
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::initialise_program()
  {

    // Init glsl_program_base
    {
      //
      wrapper::qreadfile vertex_file( ":/glsl/in_vec3.vert" );
      wrapper::qreadfile fragment_file( ":/glsl/in_color__out_color.frag" );

      // new program
      glsl_program_base.id = opengl::new_program( vertex_file.get_string(), fragment_file.get_string() );

      // get uniform on it
      glsl_program_base.uniform_mvp_id = opengl::get_uniform( glsl_program_base.id, "uniform_mvp" );
      glsl_program_base.uniform_mode_wireframe_id = opengl::get_uniform( glsl_program_base.id, "uniform_mode_wireframe" );
      glsl_program_base.uniform_color_id = opengl::get_uniform( glsl_program_base.id, "uniform_color" );
    }

    // Init glsl_program_textured2d
    {
      //
      wrapper::qreadfile vertex_file( ":/glsl/in_vec3_uv__out_uv.vert" );
      wrapper::qreadfile fragment_file( ":/glsl/in_uv__out_color.frag" );

      // new program
      glsl_program_textured2d.id = opengl::new_program( vertex_file.get_string(), fragment_file.get_string() );

      // get uniform on it
      glsl_program_textured2d.uniform_mvp_id = opengl::get_uniform( glsl_program_textured2d.id, "uniform_mvp" );
      glsl_program_textured2d.uniform_mode_wireframe_id = opengl::get_uniform( glsl_program_textured2d.id, "uniform_mode_wireframe" );
      glsl_program_textured2d.uniform_sampler2d_texture_id = opengl::get_uniform( glsl_program_textured2d.id, "uniform_sampler2d_texture" );
    }

  }

  void MeshViewerWidget::initialise_buffer()
  {
    //
    glGenVertexArrays( 1, &glsl_program_base.vao_vertex_id );
    glGenVertexArrays( 1, &glsl_program_base.vao_vertex_index_id );
    glGenVertexArrays( 1, &glsl_program_textured2d.vao_vertex_uv_index_id );
    glGenBuffers( 1, &buffer.vertex_id );
    glGenBuffers( 1, &buffer.vertex_uv_id );
    glGenBuffers( 1, &buffer.index_id );
    glGenTextures( 1, &buffer.texture_id );

    // configure vao of glsl_program_base.vao_vertex_id (vertex only)
    {
      {
        glBindVertexArray( glsl_program_base.vao_vertex_id );
        {
          glEnableVertexAttribArray( 0 );
          glBindBuffer( GL_ARRAY_BUFFER, buffer.vertex_id );
          glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, nullptr ); // glsl : layout( location = 0 ) in vec3 vertex_from_buffer;
        }
        glBindVertexArray( 0 ); // Unbind vao
      }
      // Unbind after "Unbind vao" to keep vao configuration
      glDisableVertexAttribArray( 0 );
      glBindBuffer( GL_ARRAY_BUFFER, 0 );
    }

    // configure vao of glsl_program_base.vao_vertex_index_id (vertex + index)
    {
      {
        glBindVertexArray( glsl_program_base.vao_vertex_index_id );
        {
          glEnableVertexAttribArray( 0 );
          glBindBuffer( GL_ARRAY_BUFFER, buffer.vertex_id );
          glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, nullptr ); // glsl : layout( location = 0 ) in vec3 vertex_from_buffer;
        }
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer.index_id );
        glBindVertexArray( 0 ); // Unbind vao
      }
      // Unbind after "Unbind vao" to keep vao configuration
      glDisableVertexAttribArray( 0 );
      glBindBuffer( GL_ARRAY_BUFFER, 0 );
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
    }

    // configure vao of glsl_program_textured2d.vao_vertex_uv_index_id (vertex + index)
    // @see #way3 https://stackoverflow.com/a/39684775 for more detail
    {
      {
        glBindVertexArray( glsl_program_textured2d.vao_vertex_uv_index_id );
        {
          glEnableVertexAttribArray( 0 );
          glEnableVertexAttribArray( 1 );
          glBindBuffer( GL_ARRAY_BUFFER, buffer.vertex_uv_id );
          const uint buffer_stride( sizeof( mesh::textured2d_vertex_pack ) );
          const void * const vertex_offset( reinterpret_cast<void *>( 0 ) );
          const void * const uv_offset( reinterpret_cast<void *>( sizeof( glm::vec3 ) ) );
          glVertexAttribPointer( 0, 3, GL_FLOAT, false, buffer_stride, vertex_offset ); // glsl : layout( location = 0 ) in vec3 vertex_from_buffer;
          glVertexAttribPointer( 1, 2, GL_FLOAT, false, buffer_stride, uv_offset );     // glsl : layout( location = 1 ) in vec2 uv_from_buffer;
        }
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer.index_id );
        glBindVertexArray( 0 ); // Unbind vao
      }
      {
        /** @todo see to bind texture ? */
      }
      // Unbind after "Unbind vao" to keep vao configuration
      glDisableVertexAttribArray( 0 );
      glDisableVertexAttribArray( 1 );
      glBindBuffer( GL_ARRAY_BUFFER, 0 );
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
    }

    // configure sampler of glsl_program_textured2d
    {
      glUseProgram( glsl_program_textured2d.id );
      glActiveTexture( glsl_program_textured2d.sampler2d_texture_num );
      glBindTexture( GL_TEXTURE_2D, buffer.texture_id );
      opengl::set_uniform_value( glsl_program_textured2d.uniform_sampler2d_texture_id, int( glsl_program_textured2d.sampler2d_texture_num - GL_TEXTURE0 ) );
      glUseProgram( 0 );  // Unbind program first to keep saved texture binding
      glActiveTexture( 0 );
      // @todo Do not ? why ? glBindTexture( GL_TEXTURE_2D, 0 );
    }

  }

  // ---- ---- ---- ----

  void MeshViewerWidget::throw_if_opengl_not_initialized() const
  {
    if ( !opengl_initialized )
      throw std::runtime_error( "OpenGL is not initialized" );
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::initializeGL()
  {
    makeCurrent();

    // Initialise GLEW
    {
      glewExperimental = true; // Nécessaire pour le profil core
      if ( glewInit() != GLEW_OK )
        throw std::runtime_error( "Failed to initialize GLEW\n" );
      if ( !GLEW_VERSION_3_3 )
        throw std::runtime_error( "Current GLEW Instance not support: GLEW_VERSION_3_3" );
    }

    //
    glEnable( GL_MULTISAMPLE ); // for Anti-Aliasing
    glEnable( GL_DEPTH_TEST );

    // Configure render pipeline
    {
      initialise_program();
      initialise_buffer();
    }

    //
    glClearColor( 1.0, 1.0, 1.0, 1.0 );

    //
    opengl_initialized = true;

  }


  void MeshViewerWidget::resizeGL( int _w, int _h )
  {
    camera.projection.aspect = float( _w ) / float( _h > 0.f ? _h : 1.f );
    camera.projection.compute();
    glViewport( 0, 0, _w, _h );
    updateGL();
  }



  void MeshViewerWidget::paintGL()
  {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    if (
      ( glsl_program_mode ==  glsl_program_modes::base )
      || ( glsl_program_mode ==  glsl_program_modes::base_not_indexed )
    )
    {
      // use program
      glUseProgram( glsl_program_base.id );
      opengl::set_uniform_value( glsl_program_base.uniform_mvp_id, camera.compute() );

      if ( glsl_program_mode ==  glsl_program_modes::base )
      {
        // binds buffers (vertex + index)
        glBindVertexArray( glsl_program_base.vao_vertex_index_id );

        // draw by index
        {
          opengl::set_uniform_value( glsl_program_base.uniform_mode_wireframe_id, int( 0 ) );
          glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
          glDrawElements( GL_TRIANGLES, GLint( buffer.index_count ), GL_UNSIGNED_INT, nullptr );

          if ( paint_wireframe )
          {
            opengl::set_uniform_value( glsl_program_base.uniform_mode_wireframe_id, int( 1 ) );
            glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
            glDrawElements( GL_TRIANGLES, GLint( buffer.index_count ), GL_UNSIGNED_INT, nullptr );
          }
        }
      }
      else if ( glsl_program_mode ==  glsl_program_modes::base_not_indexed )
      {
        // binds buffer (vertex only)
        glBindVertexArray( glsl_program_base.vao_vertex_id );

        // draw by vertex
        {
          opengl::set_uniform_value( glsl_program_base.uniform_mode_wireframe_id, int( 0 ) );
          glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
          glDrawArrays( GL_TRIANGLES, 0, GLint( buffer.vertex_count ) );

          if ( paint_wireframe )
          {
            opengl::set_uniform_value( glsl_program_base.uniform_mode_wireframe_id, int( 1 ) );
            glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
            glDrawArrays( GL_TRIANGLES, 0, GLint( buffer.vertex_count ) );
          }
        }
      }

      //
      glBindVertexArray( 0 ); // Unbind vao
      glUseProgram( 0 ); // Unuse program
    }
    else if ( glsl_program_mode ==  glsl_program_modes::textured2d )
    {
      // use program
      glUseProgram( glsl_program_textured2d.id );
      opengl::set_uniform_value( glsl_program_textured2d.uniform_mvp_id, camera.compute() );

      // draw by index
      {
        // binds buffers (vertex_uv + index)
        glBindVertexArray( glsl_program_textured2d.vao_vertex_uv_index_id );

        opengl::set_uniform_value( glsl_program_textured2d.uniform_mode_wireframe_id, int( 0 ) );
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
        glDrawElements( GL_TRIANGLES, GLint( buffer.index_count ), GL_UNSIGNED_INT, nullptr );

        if ( paint_wireframe )
        {
          opengl::set_uniform_value( glsl_program_textured2d.uniform_mode_wireframe_id, int( 1 ) );
          glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
          glDrawElements( GL_TRIANGLES, GLint( buffer.index_count ), GL_UNSIGNED_INT, nullptr );
        }

        glBindVertexArray( 0 ); // Unbind vao (vertex_uv + index)
      }

      glUseProgram( 0 ); // Unuse program
    }

  }

  // ---- ---- ---- ----


  void MeshViewerWidget::load_mesh( const std::vector<glm::vec3> & vertices )
  {
    throw_if_opengl_not_initialized();

    glsl_program_mode = glsl_program_modes::none; // disable for update buffer

    // BufferData of Vertex
    {
      buffer.vertex_count = GLuint( vertices.size() );
      const GLuint vertex_sizeof( buffer.vertex_count * sizeof( glm::vec3 ) );
      glBindBuffer( GL_ARRAY_BUFFER, buffer.vertex_id );
      glBufferData( GL_ARRAY_BUFFER, vertex_sizeof, vertices.data(), GL_STATIC_DRAW );
      glBindBuffer( GL_ARRAY_BUFFER, 0 ); // Unbind
    }

    glsl_program_mode = glsl_program_modes::base_not_indexed;
    updateGL();
  }

  // ---- ----

  void MeshViewerWidget::load_mesh( const mesh::base & mesh )
  {
    throw_if_opengl_not_initialized();
    glsl_program_mode = glsl_program_modes::none; // disable for update buffer

    // BufferData of Index
    /**< @todo HOW Extract properly mesh Vertex Index ? */
    {
      buffer.index_count = GLuint( mesh.n_faces() * 3 );
      std::vector<GLuint> index_data;
      index_data.reserve( buffer.index_count );

      //
      mesh::textured2d_model::ConstFaceIter face_it( mesh.faces_begin() );
      const mesh::textured2d_model::ConstFaceIter face_end_it( mesh.faces_end() );
      for ( ; face_it != face_end_it; ++face_it )
      {
        mesh::textured2d_model::ConstFaceVertexIter face_vertex_it( mesh.cfv_iter( *face_it ) );
        index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
        ++face_vertex_it;
        index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
        ++face_vertex_it;
        index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
      }

      //
      const GLuint index_sizeof( buffer.index_count * sizeof( GLuint ) );
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer.index_id );
      glBufferData( GL_ELEMENT_ARRAY_BUFFER, index_sizeof, index_data.data(), GL_STATIC_DRAW );
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 ); // Unbind
    }

    // BufferData of Vertex
    {
      //
      buffer.vertex_count = GLuint( mesh.n_vertices() );
      const GLuint vertex_sizeof( buffer.vertex_count * sizeof( mesh::textured2d_model::Point ) );
      glBindBuffer( GL_ARRAY_BUFFER, buffer.vertex_id );
      glBufferData( GL_ARRAY_BUFFER, vertex_sizeof, mesh.points(), GL_STATIC_DRAW );
      glBindBuffer( GL_ARRAY_BUFFER, 0 ); // Unbind
    }

    glsl_program_mode = glsl_program_modes::base;
    updateGL();
  }

  void MeshViewerWidget::load_mesh( const mesh::textured2d_model & mesh, const std::string & base_mesh_directory )
  {
    throw_if_opengl_not_initialized();
    glsl_program_mode = glsl_program_modes::none; // disable for update buffer

    // BufferData of Index
    /**< @todo HOW Extract properly mesh Vertex Index ? */
    {
      buffer.index_count = GLuint( mesh.n_faces() * 3 );
      std::vector<GLuint> index_data;
      index_data.reserve( buffer.index_count );

      //
      mesh::textured2d_model::ConstFaceIter face_it( mesh.faces_begin() );
      const mesh::textured2d_model::ConstFaceIter face_end_it( mesh.faces_end() );
      for ( ; face_it != face_end_it; ++face_it )
      {
        mesh::textured2d_model::ConstFaceVertexIter face_vertex_it( mesh.cfv_iter( *face_it ) );
        index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
        ++face_vertex_it;
        index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
        ++face_vertex_it;
        index_data.emplace_back( GLuint( face_vertex_it->idx() ) );
      }

      //
      const GLuint index_sizeof( buffer.index_count * sizeof( GLuint ) );
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer.index_id );
      glBufferData( GL_ELEMENT_ARRAY_BUFFER, index_sizeof, index_data.data(), GL_STATIC_DRAW );
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 ); // Unbind
    }

    // BufferData of Vertex / UV
    /**< @todo HOW Extract properly mesh Vertex UV (tex_coord_2d) witout foreach all vertex ? */
    {
      //
      buffer.vertex_uv_count = GLuint( mesh.n_vertices() );
      std::vector<mesh::textured2d_vertex_pack> vertex_uv_data;
      vertex_uv_data.reserve( buffer.vertex_uv_count );

      uint debug_count( 0 );

      //
      mesh::textured2d_model::VertexIter vertex_it( mesh.vertices_begin() );
      const mesh::textured2d_model::VertexIter vertex_end_it( mesh.vertices_end() );
      for ( ; vertex_it != vertex_end_it; ++vertex_it )
      {

        if ( debug_count < 100 )
        {

          const mesh::textured2d_vertex_pack vertex_uv(
            mesh.point( *vertex_it ),
            mesh.texcoord2D( *vertex_it )
          );


          std::cout << " vertex(" << vertex_uv.vertex.x << "," << vertex_uv.vertex.y << "," << vertex_uv.vertex.z << ")"
                    << " uv(" << vertex_uv.uv.x << "," << vertex_uv.uv.y << ")" << std::endl;
        }
        else
          ++debug_count;

        vertex_uv_data.emplace_back(
          mesh.point( *vertex_it ),      // glsl : vec3 vertex_from_buffer;
          mesh.texcoord2D( *vertex_it )  // glsl : vec2 uv_from_buffer;
        );
        /**< @todo Check if  mesh.texcoord2D( *vertex_it ) is good ? */
      }

      //
      const GLuint vertex_uv_sizeof( buffer.vertex_uv_count * sizeof( mesh::textured2d_vertex_pack ) );
      glBindBuffer( GL_ARRAY_BUFFER, buffer.vertex_uv_id );
      glBufferData( GL_ARRAY_BUFFER, vertex_uv_sizeof, vertex_uv_data.data(), GL_STATIC_DRAW );
      glBindBuffer( GL_ARRAY_BUFFER, 0 ); // Unbind
    }

    std::cout << "base_mesh_directory : " << base_mesh_directory << std::endl;

    // Texture
    {
      OpenMesh::MPropHandleT< std::map< int, std::string >> texture_property;
      std::string first_texture_entry;
      if ( mesh.get_property_handle( texture_property, "TextureMapping" ) )
        if ( !mesh.property( texture_property ).empty() )
          first_texture_entry = mesh.property( texture_property ).begin()->second;
      if ( first_texture_entry.empty() )
        std::cout << "No texture property" << std::endl;
      else
      {
        std::cout << "first_texture_entry " << first_texture_entry << std::endl;
        for ( const std::pair<int, std::string> & el : mesh.property( texture_property ) )
          std::cout << "  texture_entry n" << el.first << " : " << el.second << std::endl;

        const std::string texture_path( base_mesh_directory + "/" + first_texture_entry );
        std::cout << "opening texture_path : " << texture_path << std::endl;

        // Opening it
        QImage image( QString::fromStdString( texture_path ) );
        QImage::Format format( image.format() );

        //
        std::cout << "  Image size " << image.width() << "x" << image.height() << std::endl;

        //
        const bool compatible_format(
          ( format == QImage::Format_RGB888 )
          || ( format == QImage::Format_RGBA8888 )
        );

        if ( !compatible_format )
        {
          std::cout << "  Incompatible image format : " << format << std::endl
                    << "  Try to convert it ..." << std::endl;
          //Format_Indexed8
          image = image.convertToFormat( QImage::Format_RGB888 );
          format = image.format();
          std::cout << "  New image format : " << format << std::endl;
        }


        //
        const GLenum gl_format(
          ( format == QImage::Format_RGB888 ) ?
          GL_RGB : GL_RGBA
        );

        const GLint gl_internal_format(
          ( format == QImage::Format_RGB888 ) ?
          GL_RGB8 : GL_RGBA8
        );

        const GLenum gl_pixel(
          GL_UNSIGNED_BYTE
        );

        //
        glActiveTexture( glsl_program_textured2d.sampler2d_texture_num );
        glBindTexture( GL_TEXTURE_2D, buffer.texture_id );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexImage2D(
          GL_TEXTURE_2D,
          0, // level,
          gl_internal_format,
          static_cast<GLsizei>( image.width() ),
          static_cast<GLsizei>( image.height() ),
          0,  // border
          gl_format,
          gl_pixel,
          image.constBits()
        );
        glActiveTexture( 0 );
        // @todo Do not ? why ? glBindTexture( GL_TEXTURE_2D, 0 ); // Unbind
      }
    }

    glsl_program_mode = glsl_program_modes::textured2d;
    updateGL();
  }

  // ---- ---- ---- ----

  void MeshViewerWidget::mousePressEvent( QMouseEvent * _event )
  {
    mouse_last_position = _event->pos();
  }

  void MeshViewerWidget::mouseMoveEvent( QMouseEvent * event )
  {
    static const float mouse_ratio( 0.01f );

    //
    const QPoint mouse_new_position( event->pos() );

    //
    const glm::vec2 mouse_distance(
      mouse_new_position.x() - mouse_last_position.x(),
      mouse_new_position.y() - mouse_last_position.y()
    );

    //
    mouse_last_position = mouse_new_position;

    if ( ( event->buttons() == ( Qt::LeftButton + Qt::MidButton ) ) || ( event->buttons() == Qt::LeftButton && event->modifiers() == Qt::ControlModifier ) )
    {
      const float h( height() );
      const float value_y( mouse_distance.y * 3.f / h );
      camera.view.position.z += value_y;
      camera.view.compute();
    }
    else if ( ( event->buttons() == Qt::MidButton ) || ( event->buttons() == Qt::LeftButton && event->modifiers() == Qt::AltModifier ) )
    {
      camera.model.position.x += mouse_distance.x * mouse_ratio;
      camera.model.position.y += -mouse_distance.y * mouse_ratio;
      camera.model.compute();
      /**< @todo implement real mouse 3d mouvement */
    }
    else if ( event->buttons() == Qt::RightButton )
    {
      // right click move ?
    }
    else if ( event->buttons() == Qt::LeftButton )
    {
      // axes vector rotation :
      camera.model.rotation.y += mouse_distance.x * mouse_ratio;
      camera.model.rotation.x += mouse_distance.y * mouse_ratio;
      camera.model.compute();
      /**< @todo implement model mouse 3d sphere rotation */
    }
    updateGL();
  }

  void MeshViewerWidget::mouseReleaseEvent( QMouseEvent * /* _event */ )
  {
  }

  void MeshViewerWidget::wheelEvent( QWheelEvent * _event )
  {
    float d = -float( _event->delta() ) / 120.f * 0.2f;
    camera.view.position.z += d;
    camera.view.compute();
    updateGL();
    _event->accept();
  }


}
