#include <gui/MainWindow.h>
#include <ui_MainWindow.h>

//#include <GL/glew.h>
#include <gl/gl.h>

#include <mesh/base.h>
#include <mesh/textured2d_model.h>

#include <wrapper/qreadfile.h>

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include <iostream>
#include <fstream>

namespace gui {

  MainWindow::MainWindow( QWidget * parent ) :
    QMainWindow( parent ),
    ui( new Ui::MainWindow() )
  {
    ui->setupUi( this );
  }

  MainWindow::~MainWindow()
  {
    delete ui;
  }

  void MainWindow::showEvent( QShowEvent * event )
  {
    // call whatever your base class is!
    if ( event->spontaneous() || is_initialized )
      return;

    // When MeshViewerWidget is initialized
    on_actionOpenInternalBunny_triggered();

    //
    ui->checkBox_DrawWireframe->setChecked( ui->widget->paint_wireframe );

    //
    is_initialized = true;
  }

  // ---- ---- ---- ----


  void MainWindow::load_internal_mesh( const std::string & mesh_file )
  {
    const std::string mesh_resource_path( ":/mesh/" + mesh_file );
    std::cout << "internal loading from : " << mesh_resource_path << std::endl;

    //
    try
    {
      mesh::base mesh;

      //
      wrapper::qreadfile mesh_file( mesh_resource_path );
      std::unique_ptr<wrapper::qistream> mesh_file_stream_up( mesh_file.get_stream() );

      OpenMesh::IO::Options opt;

      //
      if ( !OpenMesh::IO::read_mesh( mesh, *mesh_file_stream_up, "obj", opt ) )
        throw std::runtime_error( "openmesh error on internal file : " + mesh_resource_path );

      //
      ui->widget->load_mesh( mesh );
    }
    catch ( const std::exception & e )
    { std::cout << "mesh loading error : " << e.what() << std::endl; }

  }

  // ---- ---- ---- ----

  //  void MainWindow::on_pushButton_2_clicked()
  //  {
  //    static const std::string path( "bunny_low_poly.obj" );

  //    // Cet exemple montre comment ouvrir un .obj
  //    mesh::base mesh;

  //    if ( !OpenMesh::IO::read_mesh( mesh, path ) )
  //    {std::cout << "can't load external file : " << path << std::endl; return;}

  //    std::cout << "external loading from : " << path << std::endl;

  //    send_mesh( mesh );
  //  }

  void MainWindow::on_actionOpenMesh_triggered()
  {
    const QString file_path_q
    {
      QFileDialog::getOpenFileName(
        this,
        "Load Mesh", "",
        "OBJ file (*.obj)"  /**< @todo ;;STL file (.stl);; ...  */
      )
    };

    //
    if ( file_path_q.isNull() ) // no file selection
      return;

    //
    try
    {
      //
      const std::string path( file_path_q.toStdString() );

      //
      QFileInfo file_info( file_path_q );
      if ( !file_info.exists() )
        throw std::runtime_error( "File \"" + path + "\" not exist" );
      if ( file_info.isDir() )
        throw std::runtime_error( "\"" + path + "\" it's not a file" );

      //
      const std::string base_mesh_directory( file_info.dir().path().toStdString() );

      //
      std::cout << "external loading from : " << path << std::endl;

      mesh::textured2d_model textured_mesh;

      OpenMesh::IO::Options opt(
        OpenMesh::IO::Options::VertexTexCoord
      );

      if ( !OpenMesh::IO::read_mesh( textured_mesh, path, opt ) )
        throw std::runtime_error( "openmesh error on external file : " + path );

      ui->widget->load_mesh( textured_mesh, base_mesh_directory );
    }
    catch ( const std::exception & e )
    {
      QMessageBox::critical(
        this, "Mesh file loading",
        "Error during mesh file loading : \n" + QString( e.what() ),
        QMessageBox::Cancel,
        QMessageBox::Cancel
      );
    }
  }

  // ---- ----

  void MainWindow::on_actionOpenInternalBunny_triggered()
  {
    load_internal_mesh( "bunny_low_poly.obj" );
  }

  // ---- ----

  void MainWindow::on_actionComputeInternalTriangle_triggered()
  {
    static const std::vector<glm::vec3> triangle_data
    {
      {-1.0f, -1.0f, 0.0f},
      {1.0f, -1.0f, 0.0f },
      {0.0f,  1.0f, 0.0f },
    };
    ui->widget->load_mesh( triangle_data );
  }

  // ---- ---- ---- ----

  void MainWindow::on_checkBox_DrawWireframe_stateChanged( int arg1 )
  {
    ui->widget->paint_wireframe = bool( arg1 );
    ui->widget->updateGL();
  }

}


