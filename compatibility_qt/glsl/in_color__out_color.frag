#version 330 core

uniform int uniform_mode_wireframe = 0;
uniform vec4 uniform_color = vec4( 0.1f, 1.f, 0.f, 1.f );

//uniform float uniform_time;

in vec3 vertex;

out vec4 color;

void main()
{

  // // Normalized pixel coordinates (from 0 to 1)
  // vec2 uv = fragCoord/iResolution.xy;
  //
  // // Time varying pixel color
  // vec3 col = 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));
  //
  // // Output to screen
  // fragColor = vec4(col,1.0);

  vec3 col = 0.5 + 0.5 * cos( vertex.xyx + vec3( 0, 2, 4 ) );

  if ( uniform_mode_wireframe == 0 ) // use uniform_color
    // color = vec4( col, 1.f);
    color = uniform_color;
  else // just color
    color = vec4( 1.f, 0.f, 0.f, 1.f );

  // if(vertex.x == -10000.f)
  //   color = uniform_color;

}
