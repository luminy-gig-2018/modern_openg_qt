#version 330 core

uniform int uniform_mode_wireframe = 0;

uniform sampler2D uniform_sampler2d_texture;

in vec2 uv;

out vec4 color;



void main()
{
  if ( uniform_mode_wireframe == 0 ) // use texture
    color = texture( uniform_sampler2d_texture, uv);
  else // just color
    color = vec4( 1.f, 0.f, 0.f, 1.f );

  //if(uv.x>=0.f)
  //  color.rg = uv;

}
